﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcCustomer;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GrpcServices
{
    public class CustomerService : GrpcCustomer.Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task GetCustomers(Empty request, IServerStreamWriter<CustomerShortDTO> responseStream, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var responseList = customers.Select(x => new CustomerShortDTO()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            foreach (var customerShortDto in responseList)
            {
               await responseStream.WriteAsync(customerShortDto);
            }
        }

        public override async Task<CustomerDTO> GetCustomer(GuidIdentifier request, ServerCallContext context)
        {

            if (Guid.TryParse(request.Id, out var id))
            {

                var customer = await _customerRepository.GetByIdAsync(id);
                var response = new CustomerDTO()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName
                };

                context.Status = new Status(StatusCode.OK, "Customer exist");

                return response;
            }

            context.Status = new Status(StatusCode.NotFound, "Customer not exist");

            return new CustomerDTO();
        }

        public override async Task<CustomerDTO> CreateCustomer(CreateOrEditCustomerDTO request, ServerCallContext context)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(x=>Guid.Parse(x.Id)).ToList());

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            var newCustomer = await _customerRepository.GetByIdAsync(customer.Id);

            if (newCustomer != null)
                context.Status = new Status(StatusCode.OK, "Customer created");

            var respponse = new CustomerDTO()
            {
                Id = newCustomer.Id.ToString(),
                Email = newCustomer.Email,
                FirstName = newCustomer.FirstName,
                LastName = newCustomer.LastName
            };
            respponse.Preferences.AddRange(newCustomer.Preferences.Select(x=>new PreferenceDTO()
            {
                Id = x.PreferenceId.ToString(),
                Name = x.Preference.Name
            }));

            return respponse;
        }

        public override async Task<Empty> EditCustomers(CreateOrEditCustomerDTO request, ServerCallContext context)
        {
            if (Guid.TryParse(request.IditId, out var id))
            {
                var customer = await _customerRepository.GetByIdAsync(id);

                if (customer == null)
                    throw new RpcException(new Status(StatusCode.NotFound, "Customer not exist"));

                var preferences = await _preferenceRepository
                    .GetRangeByIdsAsync(request.PreferenceIds.Select(x => Guid.Parse(x.Id)).ToList());

                CustomerMapper.MapFromModel(request, preferences, customer);

                await _customerRepository.UpdateAsync(customer);

                context.Status = new Status(StatusCode.OK, "Customer was edited");
                return new Empty();
            }

            throw new RpcException(new Status(StatusCode.NotFound, "Customer not exist"));
        }

        public override async Task<Empty> DeleteCustomer(GuidIdentifier request, ServerCallContext context)
        {
            if (Guid.TryParse(request.Id, out var id))
            {
                var customer = await _customerRepository.GetByIdAsync(id);

                if (customer == null)
                    throw new RpcException(new Status(StatusCode.NotFound, "Customer not exist"));

                await _customerRepository.DeleteAsync(customer);

                context.Status = new Status(StatusCode.OK, "Customer was edited");
                return new Empty();
            }

            throw new RpcException(new Status(StatusCode.InvalidArgument, "Id is not parsed"));
        }
    }
}
