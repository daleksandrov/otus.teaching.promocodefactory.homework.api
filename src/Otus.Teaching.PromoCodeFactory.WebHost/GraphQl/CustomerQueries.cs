﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQl
{
    public class CustomerQueries
    {
        [UsePaging]
        [UseFiltering]
        [UseSorting]
        public IQueryable<Customer> Customers([Service] DataContext dataContext)
        {
            return dataContext.Customers;
        }
    }
}
