﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.GrpcServices;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.GrpcServices
{
    public class CustomerServiceTest
    {
        private readonly Mock<IRepository<Customer>> _customerRepositoryMock;
        private readonly Mock<IRepository<Preference>> _preferenceRepositoryMock;
        private readonly CustomerService _customerService;

        public CustomerServiceTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _customerRepositoryMock = fixture.Freeze<Mock<IRepository<Customer>>>();
            _preferenceRepositoryMock = fixture.Freeze<Mock<IRepository<Preference>>>();
            _customerService = fixture.Build<CustomerService>().Create();

        }

    }
}
