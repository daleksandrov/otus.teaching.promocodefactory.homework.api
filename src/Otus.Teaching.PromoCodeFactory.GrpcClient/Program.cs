﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;
using GrpcCustomer;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Customers.CustomersClient(channel);

            //список Customers
            Console.WriteLine("список Customers: ");

            using var callGetCustomers = client.GetCustomers(new Empty());
            await foreach (var response in callGetCustomers.ResponseStream.ReadAllAsync())
            {
                Console.WriteLine($"{response.Id} | {response.Email} | {response.FirstName} | {response.LastName}");
            }

            //Получеине Customer по Id
            Console.WriteLine("Получеине Customer по Id");

            var callGetCustomerById = await client.GetCustomerAsync(new GuidIdentifier()
                {Id = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0" });

            Console.WriteLine($"{callGetCustomerById.Id} | {callGetCustomerById.Email} | {callGetCustomerById.FirstName} | {callGetCustomerById.LastName} | ");

            //Создание нового Customer
            Console.WriteLine("Создание нового Customer");

            var creatRequest = new CreateOrEditCustomerDTO()
            {
                Email = "newcustomer@mail.ru",
                FirstName = "Таня",
                LastName = "Васильева"
            };
            creatRequest.PreferenceIds.AddRange(new GuidIdentifier[]
            {
                new GuidIdentifier() {Id = "c4bda62e-fc74-4256-a956-4760b3858cbd"},
                new GuidIdentifier() {Id = "ef7f299f-92d7-459f-896e-078ed53ef99c"}
            });

            
            var resultCreate = await client.CreateCustomerAsync(creatRequest);

            Console.WriteLine(
                $"{resultCreate.Id} | {resultCreate.Email} | {resultCreate.FirstName} | {resultCreate.LastName}");
            foreach (var preference in resultCreate.Preferences.Select(x => x.Name).ToList())
            {
                Console.WriteLine(preference);
            }

            //Редактирование Customer
            Console.WriteLine("Редактирование Customer");

            var editRequest = new CreateOrEditCustomerDTO()
            {
                Email = "editcustomer@mail.ru",
                FirstName = "Елена",
                LastName = "Семенова",
                IditId = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0"
            };
            editRequest.PreferenceIds.AddRange(new GuidIdentifier[]
            {
                new GuidIdentifier() {Id = "c4bda62e-fc74-4256-a956-4760b3858cbd"}
            });

            try
            {
                await client.EditCustomersAsync(editRequest);
            }
            catch (RpcException e) 
            {
                Console.WriteLine($"{e.StatusCode} - {e.Message}");
            }

            //Удаление Customer
            Console.WriteLine("Удаление Customer");

            try
            {
                await client.DeleteCustomerAsync(new GuidIdentifier() { Id = resultCreate.Id });
            }
            catch (RpcException e)
            {
                Console.WriteLine($"{e.StatusCode} - {e.Message}");
            }

        }
    }
}
