﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.IntegrationTests.Api
{
    [CollectionDefinition("ApiIntegration")]
    public class TestServerCollection : ICollectionFixture<TestGrpcServerFixture>
    {
    }
}
