﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.WebHost;

namespace Otus.Teaching.PromoCodeFactory.IntegrationTests.Api
{
    public class TestGrpcServerFixture : IDisposable
    {
        private readonly TestWebApplicationFactory<Startup> _factory;
        public GrpcChannel GrpcChannel { get; }

        public TestGrpcServerFixture()
        {
            _factory = new TestWebApplicationFactory<Startup>();
            var client = _factory.CreateDefaultClient(new ResponseVersionHandler());
            GrpcChannel = GrpcChannel.ForAddress(client.BaseAddress, new GrpcChannelOptions
            {
                HttpClient = client
            });
        }

        public void Dispose()
        {
            _factory.Dispose();
        }
    }

    public class ResponseVersionHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);
            response.Version = request.Version;
            return response;
        }
    }
}
