﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;
using GrpcCustomer;
using Otus.Teaching.PromoCodeFactory.WebHost.GrpcServices;
using ProtoBuf.Grpc;
using ProtoBuf.Grpc.Client;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.IntegrationTests.Api.WebHost.Grpc
{
    [Collection("ApiIntegration")]
    public class CustomerServiceTest
    {
        private readonly GrpcChannel _channel;

        public CustomerServiceTest(TestGrpcServerFixture testGrpcServerFixture)
        {
            _channel = testGrpcServerFixture.GrpcChannel;
        }

        [Fact]
        public async Task GetCustomersServerStreamEndpoint_CustomersExisted_ShouldNotEmptyCustomerList()
        {
            //Arrange
            var customerClient = new Customers.CustomersClient(_channel);

            //Act
            var responseMessage = customerClient.GetCustomers(new Empty());

            //Assert
            var resultList = new List<CustomerShortDTO>();

            while (await responseMessage.ResponseStream.MoveNext())
            {
                var value = responseMessage.ResponseStream.Current;
                value.Should().BeAssignableTo<CustomerShortDTO>();
                resultList.Add(value);
            }

            resultList.Should().HaveCountGreaterThan(0);
        }

        [Fact]
        public async Task GetCustomerAsync_CustomerExisted_ShouldReturnExpectedCustomer()
        {
            //Arrange
            var customerClient = new Customers.CustomersClient(_channel);
            var expected = new CustomerDTO()
            {
                Id = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0",
                Email = "ivan_sergeev@mail.ru",
                FirstName = "Иван",
                LastName = "Петров"
            };
            
            //Act
            var responseMessage = await customerClient.GetCustomerAsync(new GuidIdentifier() { Id = expected.Id });

            //Assert
            responseMessage.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task CreateCustomerAsync_CanCreateCustomer_ShouldCreateExpectedCustomer()
        {
            //Arrange
            var customerClient = new Customers.CustomersClient(_channel);
            var createRequest = new CreateOrEditCustomerDTO()
            {
                Email = "newcustomer@mail.ru",
                FirstName = "Таня",
                LastName = "Васильева"
            };
            createRequest.PreferenceIds.AddRange(new GuidIdentifier[]
            {
                new GuidIdentifier() {Id = "c4bda62e-fc74-4256-a956-4760b3858cbd"},
                new GuidIdentifier() {Id = "ef7f299f-92d7-459f-896e-078ed53ef99c"}
            });
            var preferenceIdList = createRequest.PreferenceIds.Select(x => x.Id).ToList();

            //Act
            var resultCreate = await customerClient.CreateCustomerAsync(createRequest);

            //Assert
            resultCreate.Email.Should().Be(createRequest.Email);
            resultCreate.FirstName.Should().Be(createRequest.FirstName);
            resultCreate.LastName.Should().Be(createRequest.LastName);
            resultCreate.Preferences.Should()
                .Contain(x=> preferenceIdList.Any(y=>y.Equals(x.Id)));
        }
    }
}
 